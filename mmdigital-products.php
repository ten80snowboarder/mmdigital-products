<?php
/*
Plugin Name:	M & M Digital Printing - Products
Plugin URI:		http://ten80snowboarder.com
Description:	Import, display, order, and status for M & M Digital Printing products
Author:			ten80snowboarder
Version:		17.8.30.2
Author URI:		http://ten80snowboarder.com
Text Domain:	mm-product-import
*/

defined( 'ABSPATH' ) or die( 'No direct access!' );

//	ADD ADMIN MENU ITEM
add_action( 'admin_menu', 'mmdigital_products_menu' );
function mmdigital_products_menu() {
	add_menu_page( 'Product Import', 'Product Import', 'manage_options', 'product-import', 'mmdigital_products_import_panel' );
}

//	DISPLAY THE PANEL FOR THE UPLOAD
function mmdigital_products_import_panel() {

	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	$setup_datatables = mmdigital_setup_datatables();
	$handle_post = mmdigital_products_import_handle_post();

	?>
	<div class="wrap">

		<h2><?php echo __( 'Product Import', 'mm-product-import' ); ?></h2>
		<p>Use the below file upload to import the Sales Data CSV into the database.</p>

		<form name="frmImport" method="post" enctype="multipart/form-data">

			<input type="hidden" name="product_import" value="1" />
			<p><?php _e("Product Import Type:", 'mm-product-import' ); ?>
				<select name="category">
					<option value="">Select a category...</option>
					<option value="business-cards" selected="selected">Business Cards</option>
				</select>
			</p>

			<p><?php _e("Product Import CSV:", 'mm-product-import' ); ?>
				<input type="file" name="product_file" size="20" />
				<?php wp_nonce_field( 'product_file', 'product_file_nonce' ); ?>
			</p>

			<p class="submit">
				<button type="submit" name="submit" class="button button-submit button-primary" disabled="disabled"><?php esc_attr_e('Start Import') ?></button>
			</p>

		</form>

	</div>

	<script>
	(function($){

		$('input[name=product_file]').on( 'change', function() {
			// is this a CSV?
			var filepath = $(this).val();
				filetype = (/[.]/.exec(filepath)) ? /[^.]+$/.exec(filepath) : undefined;

			if( filetype[0] == 'csv' ){
			//	is a CSV, enable the button
				$('.button-submit').removeAttr('disabled');
			}else{
			//	not a CSV, disable the button
				$('.button-submit').attr('disabled', 'disabled');
			}
		});

	})(jQuery);
	</script>

<?php
}	//	end mmdigital_products_import_panel()

function mmdigital_products_import_handle_post(){

	//	if a product file has been uploaded
	if( isset( $_FILES['product_file'], $_POST['product_file_nonce'], $_POST['category'] ) ):

		$csv = $_FILES['product_file'];
		$code = $_POST['product_file_nonce'];
		$category = $_POST['category'];

		//	Use the wordpress function to upload
		//	product_file corresponds to the position in the $_FILES array
		//	0 means the content is not associated with any other posts
		$uploaded = media_handle_upload( 'product_file', 0 );

		//	Error checking using WP functions
		if( is_wp_error( $uploaded ) ):
			error_log('Product Import - Error Uploading File: ' . $uploaded->get_error_message());
			return "Error uploading file: " . $uploaded->get_error_message();
		
		else:

			//	get the file path
			$file_path	= get_attached_file( $uploaded );

			//	file was uploaded OK, now read, loop, insert (update on duplicate key) and then remove the file if all OK
			$proc_file = import_product_data( $file_path, $code, $category );

			//	DELETE the file, we don't need it now...
			wp_delete_attachment( $uploaded, TRUE );

		endif;

	endif;
	return false;

}	//	end mmdigital_products_import_handle_post()

/**
 * Returns current plugin version.
 *
 * @return string Plugin version
 */
function plugin_get_version(){
	if( !function_exists( 'get_plugins' ) )
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	$plugin_folder = get_plugins( '/' . plugin_basename( dirname( __FILE__ ) ) );
	$plugin_file = basename( ( __FILE__ ) );
	return $plugin_folder[$plugin_file]['Version'];
}

function mmdigital_setup_datatables(){

	global $wpdb;
	$db_version = get_option( 'db_version' );
	$plugin_version = plugin_get_version();
	$oldVersion = get_option( 'mm_products_db_version', '1.0' );
	$newVersion = $db_version.'-'.$plugin_version;

	$charset_collate = $wpdb->get_charset_collate();
	$table_name = $wpdb->prefix . 'mm_products';
	if( ( $wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name ) || ( $oldVersion != $newVersion ) ):
		//	the formate of this statement is VERY important. Don't screw it up!
		$sqlCreate = "CREATE TABLE $table_name (
				id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
				product_name VARCHAR(255) NULL DEFAULT NULL,
				category VARCHAR(255) NULL DEFAULT NULL,
				p100 FLOAT(11,2) NULL DEFAULT NULL,
				p250 FLOAT(11,2) NULL DEFAULT NULL,
				p500 FLOAT(11,2) NULL DEFAULT NULL,
				p1000 FLOAT(11,2) NULL DEFAULT NULL,
				p2500 FLOAT(11,2) NULL DEFAULT NULL,
				p5000 FLOAT(11,2) NULL DEFAULT NULL,
				PRIMARY KEY (`id`),
				UNIQUE KEY `product_name` (`product_name`),
				INDEX `category_index` (`category`)
			) $charset_collate";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sqlCreate );

		update_option( 'mm_products_db_version', $newVersion );
	endif;

	return $newVersion;

}

function import_product_data( $file_path, $code, $category ){

	//	open the CSV
	if( ( $handle = fopen( $file_path, "r") ) !== FALSE ):

		$data = array();
		//	loop the lines in the file
		while( ( $row = fgetcsv( $handle, 10000, ";" ) ) !== FALSE ):

			//	ignore first row -- column headers
			if( substr( $row[0], 0, 5 ) != 'name,' ):

				//	explode the row into an array of columns
				$cells = explode( ',', $row[0] );
				array_push( $data, $cells );
				$cellcnt = count( $cells );

			endif;

		endwhile;
	//	new dBug( $cellcnt );
	//	new dBug( $data );

		$insert_update = insert_update( $data, $cellcnt, $category );
	//	new dBug( $insert_update );

	endif;

	return true;

}	//	end import_product_data()

function insert_update( $data, $cellcnt, $cat ){

	global $wpdb;

	$dbpush = 0;
	$table_name = $wpdb->prefix . 'mm_products';

	foreach( $data as $r ):

		$pname = $r[0];
		array_shift( $r );
		new dBug( $r );

		$sqlInsertUpdate = "INSERT INTO $table_name (";

		switch( $cat ):

			case 'business-cards';
				$insertFields = "
					product_name,category,p100,p250,p500,p1000,p2500,p5000";
				$insertValues = "
					'$pname','$cat','$r[0]','$r[1]','$r[2]','$r[3]','$r[4]','$r[5]'";
				$updateVars = "
					p100 = '$r[0]',
					p250 = '$r[1]',
					p500 = '$r[2]',
					p1000 = '$r[3]',
					p2500 = '$r[4]',
					p5000 = '$r[5]'
				";
				$runQry = true;
				break;

			defaultcase;
				$runQry = false;
				break;

		endswitch;

		$sqlInsertUpdate .= $insertFields . ") VALUES (";
		$sqlInsertUpdate .= $insertValues . ") ON DUPLICATE KEY UPDATE ";
		$sqlInsertUpdate .= $updateVars . ";";

		if( $runQry ):

			$dbpush = $wpdb->query( $sqlInsertUpdate );

		endif;

		new dbug( $sqlInsertUpdate );

	endforeach;

	new dBug( $dbpush );

}

/*
COUNTS =============================================================
Business Cards			100 	250 	500 	1000	2500	5000
Rack Cards						250 	500 	1000	2500	5000
Bookmarks				100 	250 	500 	1000	2500	5000
Post Cards				100 	250 	500 	1000	2500	5000
Thank You Cards			100 	250 	500 	1000	2500	5000
Flyers Brochures		100 	250 	500 	1000	2500	5000
Presentation Folders			250 	500 	1000
Door Hangers					250 	500 	1000	2500	5000
Forms					100 	250 	500
*/












